package com.ecosim125.ppt;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class CommandListener implements CommandExecutor{

	private Main main;
	private FileConfiguration config;
	private Teleporter t;
	
	public CommandListener(Main m,Teleporter t) {
		main = m;
		this.t = t;
		config = main.getFileConfiguration();
	}
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!command.getName().equals("ppt")) {
			return false;
		} else if (args.length!=1) {
			sender.sendMessage("Fehlerhafte Argumentzahl. Erlaubt:");
			sender.sendMessage("/ppt set");
			sender.sendMessage("/ppt delete");
			return false;
		} else if (!(sender instanceof Player)) {
			sender.sendMessage("Befehl kann nicht von der Console gegeben werden");
			return false;
		} else if (!((Player)sender).hasPermission("ppt.edit")) {
			sender.sendMessage("Dies ist dir nicht gestattet");
			return false;
		}
		if (args[0].equals("set")){
			switch(((Player)sender).getItemInHand().getType()){
			case STONE_PLATE:
				setTargetLocation(((Player)sender),"stone");
				break;
			case WOOD_PLATE:
				setTargetLocation(((Player)sender),"wood");
				break;
			case GOLD_PLATE:
				setTargetLocation(((Player)sender),"gold");
				break;
			case IRON_PLATE:
				setTargetLocation(((Player)sender),"iron");
				break;
			default:
				sender.sendMessage("Du musst eine Druckplatte in der Hand halten");
				break;
			}
			return true;
		}else if (args[0].equals("delete")){
			switch(((Player)sender).getItemInHand().getType()){
			case STONE_PLATE:
				remove(((Player)sender),"stone");
				break;
			case WOOD_PLATE:
				remove(((Player)sender),"wood");
				break;
			case GOLD_PLATE:
				remove(((Player)sender),"gold");
				break;
			case IRON_PLATE:
				remove(((Player)sender),"iron");
				break;
			default:
				sender.sendMessage("Du musst eine Druckplatte in der Hand halten");
				break;
			}
			return true;
		}
		sender.sendMessage("Unbekanntes Argument. Bekannt:");
		sender.sendMessage("/ppt set");
		sender.sendMessage("/ppt delete");
		return false;
	}

	private void remove(Player sender,String string) {
		config.set(string, null);
		try {
			config.save(new File(main.getDataFolder()+File.separator+"config.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		t.invalidate(string);
		sender.sendMessage("Ziel erfolgreich entfernt");
	}
	private void setTargetLocation(Player sender, String material) {
		Location l = sender.getLocation();
		config.set(material+".world", l.getWorld().getName());
		config.set(material+".x", l.getBlockX());
		config.set(material+".y", l.getBlockY());
		config.set(material+".z", l.getBlockZ());
		config.set(material+".pitch", l.getYaw());
		config.set(material+".yaw", l.getPitch());
		try {
			config.save(new File(main.getDataFolder()+File.separator+"config.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		t.invalidate(material);
		main.getLogger().log(Level.INFO, sender.getName()+" set new destination for "+material+": "+l);
		sender.sendMessage("Ziel erfolgreich gesetzt");
	}
	
}
