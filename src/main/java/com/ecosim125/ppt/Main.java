package com.ecosim125.ppt;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Hello world!
 *
 */
public class Main extends JavaPlugin {
	private FileConfiguration config;
	
	@Override
	public void onEnable() {
		loadConfig();

		Teleporter teleporter = new Teleporter(this);
		getServer().getPluginManager().registerEvents(teleporter, this);
		getCommand("ppt").setExecutor(new CommandListener(this,teleporter));
	}

	private void loadConfig() {
		config = getConfig();
		config.options().copyDefaults(true);
	}

	public FileConfiguration getFileConfiguration() {
		if (config == null) {
			synchronized (this) {
				if (config == null)
					loadConfig();
			}
		}
		return config;
	}
}
