package com.ecosim125.ppt;

import java.util.concurrent.ExecutionException;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.google.common.base.Optional;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class Teleporter implements Listener {

	private final LoadingCache<String, Optional<Location>> targetLocations = CacheBuilder.newBuilder()
			.concurrencyLevel(1).build(new CacheLoader<String, Optional<Location>>() {

				@Override
				public Optional<Location> load(String key) throws Exception {
					return Optional.fromNullable(loadTarget(key));
				}
			});
	private Main main;
	private FileConfiguration config;

	public Teleporter(Main main) {
		this.main = main;
		config = main.getFileConfiguration();
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent ev) {
		if (!ev.getPlayer().hasPermission("ppt.use"))
			return;
		if (ev.getAction().equals(Action.PHYSICAL)) {
			Location target = null;
			switch (ev.getClickedBlock().getType()) {
			case STONE_PLATE:
				target = getTargetLocation("stone");
				break;
			case WOOD_PLATE:
				target = getTargetLocation("wood");
				break;
			case GOLD_PLATE:
				target = getTargetLocation("gold");
				break;
			case IRON_PLATE:
				target = getTargetLocation("iron");
				break;
			default:
				break;
			}
			if (target != null) {
				ev.getPlayer().teleport(target);
				main.getLogger().log(Level.INFO, ev.getPlayer().getName()+" wurde teleportiert("+ev.getClickedBlock()+" -> "+target+")");
				ev.setCancelled(true);
			}
		}
	}

	private Location getTargetLocation(String material) {
		try {
			return targetLocations.get(material).orNull();
		} catch (ExecutionException e) {
			e.printStackTrace();
			return null;
		}
	}

	private Location loadTarget(String material) {
		if (!config.contains(material)) {
			return null;
		}

		World world = Bukkit.getWorld(config.getString(material + ".world"));
		int x = config.getInt(material + ".x");
		int y = config.getInt(material + ".y");
		int z = config.getInt(material + ".z");
		float yaw = (float) config.getDouble(material + ".yaw");
		float pitch = (float) config.getDouble(material + ".pitch");

		return new Location(world, x, y, z, pitch, yaw);
	}

	public void invalidate(String string) {
		targetLocations.invalidate(string);
	}
}
